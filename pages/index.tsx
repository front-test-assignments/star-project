import Game from '../components/Game'
import GameArea from '../components/StarMaker/StarMaker'

export default function Home() {
  return (
    <>
      <Game/>
    </>
  )
}
