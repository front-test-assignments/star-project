import { useState, useEffect, useRef } from 'react'
import styled from '@emotion/styled'
import { clearInterval } from 'timers';
import GameArea from '../StarMaker/StarMaker';
import StarMaker from '../StarMaker/StarMaker'

export interface StarParameters {
    top: number,
    currentLeft: number,
    score: number
}

const TICK_DELAY = 500;

const GameContainer = styled.div(_props => ({
    display: 'flex',
    flexFlow: 'row',
    background: 'url(/background.png) no-repeat', 
    backgroundSize: '100% auto',
    height: '100%'
}))

const Count = styled.div(_props => ({
    color: 'salmon'
}))

const ButtonArea = styled.div(_props => ({
    width: '15%'
}))

const StarsArea = styled.div(_props => ({
    width: '70%'
}))

const TimerArea = styled.div(_props => ({
    font: 'caption',
    fontSize: '40px',
    
    width: '15%'
}))


const Game = () => {
    const [tickTimeoutHandler, setTickTimeoutHandler] = useState<NodeJS.Timeout>()
    const [hasGameStarted, setHasGameStart] = useState(false);
    const [isGamePaused, setIsGamePaused] =   useState(false);
    const [stars, setStars] =                 useState<StarParameters[]>([])
    const [timerValue, setTimerValue] =       useState(0)
    const [score, setScore] =                 useState(0)
    const initTime = new Date()
    const [gameStartedAt, setGameStartedAt] = useState(initTime)
    const [currentTime, setCurrentTime]     = useState(initTime)
    const [timeOnPause, setTimeOnPause]     = useState(0)
    const [oldStars, setOldStars]           = useState<StarParameters[]>([])

    const containerRef = useRef<HTMLDivElement>(null);    

    const monitorWidth = () => {
        if (containerRef && containerRef.current) {
            return (containerRef.current.clientWidth)
        }
        return 0
    }

    const properties = {min: -5, max: 5, leftMin: 0, leftMax: monitorWidth()*0.7}
    const {min, max, leftMin, leftMax} = properties

    useEffect(() => {
        if (hasGameStarted) {
            setGameStartedAt(new Date())
            setCurrentTime(new Date())
            makeNewStar(0)
            makeNewStar(-100)
            makeNewStar(-200)
            setTimeout(gameTick, TICK_DELAY);
        }
    }, [hasGameStarted])

    useEffect(() => {
        setTimerValue(Math.floor((currentTime.getTime() - gameStartedAt.getTime())/1000))
    }, [currentTime, gameStartedAt])

    useEffect(() => {
        if (!hasGameStarted || isGamePaused) {
            return
        }

        setTickTimeoutHandler(setTimeout(gameTick, TICK_DELAY));

        setStars((stars) => {
            collectStars(stars)
            return stars
                .filter(({top}) => top < (monitorHeight() ))
                .map(star => ({...star, top: star.top + 10}))
        });
    }, [currentTime, hasGameStarted, isGamePaused])

    const collectStars = (stars: StarParameters[]) => {
        const starsToCollect = stars.filter(({top}) => top >= monitorHeight())
        const scoreIncrement = starsToCollect.map(({score}) => score).reduce(((prev, cur) => prev + cur), 0)
        setScore((score) => score + scoreIncrement)
    }
        
    const handleStartButtonClick = () => {
        if (!hasGameStarted) {
            setHasGameStart(true)
        }
    }
    
    const handlePauseButtonClick = () => {
        if (hasGameStarted && !isGamePaused) {
            setIsGamePaused(true)
            if (tickTimeoutHandler) {
                clearTimeout(tickTimeoutHandler)
            }
        }
    }
    
    const handleContinueButtonClick = () => {
        if (hasGameStarted && isGamePaused) {
            setIsGamePaused(false)
        }
    }
    
    const renderStartButton = () => {
        if (!isGamePaused) {
            return  <button onClick={handleStartButtonClick}>Запуск</button>
        }
    }
    
    const renderContinueButtron = () => {
        if (hasGameStarted && isGamePaused) {
            return <button onClick={handleContinueButtonClick}>Продолжить</button>
        }
    }
    
    const renderPauseButton = () => {
        return <button onClick={handlePauseButtonClick}>Пауза</button>
    }
    
    const getRandomNumber = (min: number, max: number) : number => {
        const result = Math.round(min + (max - min) * Math.random())
        if (result !== 0) {
            return result;
        } else {
            return getRandomNumber(min, max)
        } 
    }
    
    const getRandomPosition = (leftMin: number, leftMax: number) : number => {
        return Math.round(leftMin + (leftMax - leftMin) * Math.random())
    }
    

        
    const makeNewStar = (topOffset = 0) => {
        const newStar: StarParameters = {
            score: getRandomNumber(min, max), 
            currentLeft: getRandomPosition(leftMin, leftMax), 
            top: topOffset
        }
        setStars((stars) => [...stars, newStar])
    }

    const monitorHeight = () => {
        if (containerRef && containerRef.current) {
            return (containerRef.current.offsetHeight - 110)
        }
        return 0
    }
    
    const gameTick = () => {
        setCurrentTime(new Date())
    }

    const renderStars = () => {
        if (hasGameStarted) {
            return stars.filter(({top}) => top >= 0).map((star) => <StarMaker score={star.score} currentLeft={star.currentLeft} top={star.top}/>)      
        }
    }   
    
    const renderTimer = () => {
        return <Count>{timerValue}</Count>
    }

    const renderScore = () => {
        return <Count>{score}</Count>
    }

    const restartClickHandler = () => {
        setHasGameStart(false)
        setIsGamePaused(false)
        setStars([])
        setScore(0)
        setTimerValue(0)
    }

    return (
        <>
            <GameContainer ref={containerRef}>
                <ButtonArea>
                    { renderStartButton() }
                    { renderContinueButtron() }
                    { renderPauseButton() }
                    <button onClick={restartClickHandler}>Рестарт</button>
                </ButtonArea>
                <StarsArea>
                    { renderStars() }
                </StarsArea>
                <TimerArea>
                    { renderTimer() }
                    { renderScore() }
                </TimerArea>
            </GameContainer>
        </>
        
    )
}

export default Game