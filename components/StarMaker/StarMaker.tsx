import styled from '@emotion/styled'
import { StarParameters } from '../Game/Game'

// Rename StarMaker => Star
const StarMaker = ({top, currentLeft, score}: StarParameters) : JSX.Element => {
       // console.log(top, currentLeft, score)
    const Container = styled.div(_props => (
        {
            display: 'flex',
            height: '100px',
            left: `${currentLeft}px`,
            position: 'relative',
            top: `${top}px`,
            width: '100px',
            background: 'url("/star.png")',
            backgroundRepeat: 'no-repeat',
            backgroundSize: '100% auto',
            justifyContent: 'center',
            alignItems: 'center'
        }
    ))

    const ScoreContainer = styled.span(_props => ({
        font: 'caption',
        fontSize: '40px',
        display: 'block'
    }))

    return (
        <Container>
            <ScoreContainer>
                {score}
            </ScoreContainer>
        </Container>
    )
};

export default StarMaker