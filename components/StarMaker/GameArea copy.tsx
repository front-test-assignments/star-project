import { useState } from "react";

export default () => {
    const [top, setTop] = useState('1px')
    const delay = 20
    let i = 0
    const starTimer = () => {
        const star = document.getElementById('star'),
            bottom = star.offsetTop;
        if (i < 60) {
            setTimeout(starTimer, delay)
            setTop(bottom + 10 + 'px')
            star.style.top = bottom + 10 + 'px';
        };
        i++; 
    };
    const start = () => {
        setTimeout(starTimer, delay);
    }
    const restart = () => {
        const star = document.getElementById('star')
        // setTop('1px')
        star.style.top = '1px';
    }
    return (
        <>
        
        <button onClick={start}>Старт</button>
        <button onClick={restart}>Заново</button>
        <img id='star' src='/star.png' alt='star'
        style={
            {
                // backgroundColor:'black',
                height: '100px',
                left:'600px',
                position: 'absolute',
                top: top,
                width: '100px',
                // borderRadius: '50%'
            }
        }
        >
    </img>
    </>)
    // const timer = setTimeout(starTimer, delay);
};
